/******************************************************************************
 * timber - C logging and diagnostic output routines (quiet demos).
 * Sun Jul 21 16:23:09 BST 2013
 * Copyright (C) 2013-2016 by Iain Nicholson <iain.j.nicholson@gmail.com>
 *
 * This file is part of timber.
 *
 * timber is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * timber is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with timber; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 2013-07-22 Separate initialise and reset functions.
 * 2014-06-02 Quiet macro for unit testing.
 *
 ******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#define TDR_UNIT_TEST_QUIET
#include "timber.h"

#define USLEEP_PERIOD   1000

int main(void)
{
    int32_t i;

    TDR_DIAG_TRACE();

    TDR_DIAG_INFO("This is some information.\n");
    TDR_DIAG_INFO("The time has not been initialised.\n");
    TDR_DIAG_ERROR("This is some more information.\n");

    TDR_TIME_INIT();
    TDR_DIAG_INFO("The time has been initialised.\n");

    for (i = 0; i < 10; i++)
    {
        TDR_DIAG_INFO(" i %d\n", i);
        usleep(USLEEP_PERIOD);
    }


    usleep(USLEEP_PERIOD);
    TDR_TIME_INIT();
    usleep(USLEEP_PERIOD);
    TDR_DIAG_INFO("The time was already initialised. It shouldn'y have changed.a\n");
    usleep(USLEEP_PERIOD);

    TDR_DIAG_INFO("Resetting the time.\n");
    TDR_TIME_RESET();

    for (i = 0; i < 10; i++)
    {
        TDR_DIAG_INFO(" i %d\n", i);
        usleep(USLEEP_PERIOD);
    }

    TDR_DIAG_INFO("Done.\n");
    return 0;
}

