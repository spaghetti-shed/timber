/******************************************************************************
 * timber - C logging and diagnostic output routines.
 * Sun Jul 21 16:23:09 BST 2013
 * Copyright (C) 2013 by Iain Nicholson <iain.j.nicholson@gmail.com>
 *
 * This file is part of timber.
 *
 * timber is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * timber is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with timber; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 2013-07-22 Separate initialise and reset functions.
 *
 ******************************************************************************/

#include <sys/time.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include "timber.h"

#define DEFAULT_OUTPUT_STREAM   stdout

static FILE *output = (FILE *)-1;
static struct timeval start;
static struct timeval current;
static struct timeval delta;
static int32_t initialised;

int32_t timber_get_current_time(void);

FILE *timber_get_output(void)
{
    FILE *result = output;

    if ((FILE *)-1 == result)
    {
        result = DEFAULT_OUTPUT_STREAM;
    }

    return result;
}

void timber_set_output(FILE *stream)
{
    output = stream;
}

int32_t timber_get_start_time(void)
{
    int32_t retcode = 0;

    if (initialised)
    {
    }
    else if (0 != gettimeofday(&start, NULL))    
    {
        fprintf(stderr, "timber_get_start_time(): gettimeofday() failed.\n");
    }
    else
    {
        initialised = 1;
    }

    return retcode;
}

int32_t timber_reset_start_time(void)
{
    int32_t retcode = 0;

    if (0 != gettimeofday(&start, NULL))    
    {
        fprintf(stderr, "timber_get_start_time(): gettimeofday() failed.\n");
    }
    else
    {
        initialised = 1;
    }

    return retcode;
}


int32_t timber_get_current_time(void)
{
    int32_t retcode = 0;

    if (0 != gettimeofday(&current, NULL))    
    {
        fprintf(stderr, "timber_get_current_time(): gettimeofday() failed.\n");
    }

    return retcode;
}

int32_t timber_get_delta_time(void)
{
    int retcode = 0;

    if (0 != (retcode = timber_get_current_time()))
    {
    }
    else if (initialised)
    {
        timersub(&current, &start, &delta);
    }
    else
    {
        delta.tv_sec  = current.tv_sec;
        delta.tv_usec = current.tv_usec;
    }

    return retcode;
}

int32_t timber_get_delta_time_secs(void)
{
    timber_get_delta_time();
    return delta.tv_sec;
}

int32_t timber_get_delta_time_usecs(void)
{
    return delta.tv_usec;
}

