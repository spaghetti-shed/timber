#
# Makefile for timber - C logging and diagnostic output routines.
# Copyright (C) 2013-2022 by Iain Nicholson. <iain.j.nicholson@gmail.com>
#
# This file is part of timber.
#
# timber is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.
#
# timber is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with timber; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
# USA
#
#
# Sun Jul 21 16:23:09 BST 2013
#
# 2014-04-22 Shared library target.
# 2014-06-02 Quiet macro for unit testing.
# 2022-06-14 Add uninstall target.
#
CC=gcc
CFLAGS=-Wall -Werror -O -fPIC
SHAREDFLAGS=-shared
CPPTESTFLAGS=-DTESTING -DUNIT_TEST
TOPDIR=/home/iain/c
INCPATH = $(TOPDIR)
INSTALL_DIR=/usr/local
INSTALL_INC=$(INSTALL_DIR)/include
INSTALL_LIB=$(INSTALL_DIR)/lib64

all: test demo timber shared

test: timber_test
	./timber_test

demo: timber_demo timber_quiet_demo

main: timber

timber: timber.h timber.c
	$(CC) $(CFLAGS) -c -I$(INCPATH) timber.c

shared: timber test
	$(CC) $(CFLAGS) $(SHAREDFLAGS) -o libtimber.so timber.o

timber_test: timber_test.c timber.c timber.h
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -I$(INCPATH) -o timber_testing.o -c timber.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -I$(INCPATH) -c timber_test.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -o timber_test timber_test.o timber_testing.o $(LIBS)

timber_demo: timber_demo.c timber.c timber.h
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -I$(INCPATH) -o timber_demoing.o -c timber.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -I$(INCPATH) -c timber_demo.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -o timber_demo timber_demo.o timber_demoing.o $(LIBS)

timber_quiet_demo: timber_quiet_demo.c timber.c timber.h
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -I$(INCPATH) -o timber_demoing.o -c timber.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -I$(INCPATH) -c timber_quiet_demo.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -o timber_quiet_demo timber_quiet_demo.o timber_demoing.o $(LIBS)

install: shared
	mkdir -p $(INSTALL_INC)
	cp timber.h $(INSTALL_INC)
	mkdir -p $(INSTALL_LIB)
	cp libtimber.so $(INSTALL_LIB)

uninstall:
	rm -f $(INSTALL_INC)/timber.h
	rm -f $(INSTALL_LIB)/libtimber.so

clean:
	rm -f *.o
	rm -f timber
	rm -f timber_test
	rm -f timber_demo
	rm -f timber_quiet_demo
	rm -f libtimber.so

