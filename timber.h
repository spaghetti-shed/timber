/******************************************************************************
 * timber - C logging and diagnostic output routines (header).
 * Sun Jul 21 16:23:09 BST 2013
 * Copyright (C) 2013-2023 by Iain Nicholson <iain.j.nicholson@gmail.com>
 *
 * This file is part of timber.
 *
 * timber is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * timber is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with timber; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * 2013-07-22 Separate initialise and reset functions.
 *            Output macro with no source module, function name or line no.
 * 2014-06-02 Add TDR_UNIT_TEST_QUIET macro as a quick hack to silence output
 *            during unit testing.
 * 2023-01-07 Add TDR_DIAG_TRACEV to print line number.
 *
 ******************************************************************************/

#include <stdio.h>

#ifndef __TIMBER_H__
#define __TIMBER_H__

#ifdef __cplusplus
extern "C"
{
#endif

FILE *timber_get_output(void);
void timber_set_output(FILE *stream);
int32_t timber_get_start_time(void);
int32_t timber_reset_start_time(void);
int32_t timber_get_delta_time(void);
int32_t timber_get_delta_time_secs(void);
int32_t timber_get_delta_time_usecs(void);

#ifndef TDR_UNIT_TEST_QUIET

#define TDR_TIME_INIT() \
  timber_get_start_time();

#define TDR_TIME_RESET() \
  timber_reset_start_time();

#define TDR_SET_OUTPUT(stream) \
  timber_set_output(stream);

#define TDR_DIAG_TRACE() \
  fprintf(timber_get_output(), "%d.%06d:%s:%s()\n", timber_get_delta_time_secs(), timber_get_delta_time_usecs(), __FILE__, __FUNCTION__);

#define TDR_DIAG_TRACEV() \
  fprintf(timber_get_output(), "%d.%06d:%s:%s:%d\n", timber_get_delta_time_secs(), timber_get_delta_time_usecs(), __FILE__, __FUNCTION__, __LINE__);

#define TDR_DIAG_INFO(...) \
  fprintf(timber_get_output(), "%d.%06d:%s:%s:%d:", timber_get_delta_time_secs(), timber_get_delta_time_usecs(), __FILE__, __FUNCTION__, __LINE__); \
  fprintf(timber_get_output(), ## __VA_ARGS__);

#define TDR_DIAG_TERSE(...) \
  fprintf(timber_get_output(), ## __VA_ARGS__);

#define TDR_DIAG_ERROR(...) \
  fprintf(timber_get_output(), "%d.%06d:%s:%s:%d:ERROR:", timber_get_delta_time_secs(), timber_get_delta_time_usecs(), __FILE__, __FUNCTION__, __LINE__); \
  fprintf(timber_get_output(), ## __VA_ARGS__);

#else
#define TDR_TIME_INIT() \
  /* TDR_TIME_INIT() Quiet. */

#define TDR_TIME_RESET() \
  /* TDR_TIME_RESET() Quiet. */

#define TDR_SET_OUTPUT(stream) \
  /* TDR_SET_OUTPUT() Quiet. */

#define TDR_DIAG_TRACE() \
  /* TDR_DIAG_TRACE() Quiet. */

#define TDR_DIAG_TRACEV() \
  /* TDR_DIAG_TRACE() Quiet. */

#define TDR_DIAG_INFO(...) \
  /* TDR_DIAG_INFO() Quiet. */

#define TDR_DIAG_TERSE(...) \
  /* TDR_DIAG_TERSE() Quiet. */

#define TDR_DIAG_ERROR(...) \
  /* TDR_DIAG_ERROR() Quiet. */

#endif /* TDR_UNIT_TEST_QUIET */

#ifdef __cplusplus
}
#endif

#endif /* __TIMBER_H__ */

