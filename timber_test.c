/******************************************************************************
 * timber - C logging and diagnostic output routines (tests).
 * Sun Jul 21 16:23:09 BST 2013
 * Copyright (C) 2013-2016 by Iain Nicholson <iain.j.nicholson@gmail.com>
 *
 * This file is part of timber.
 *
 * timber is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * timber is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with timber; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 ******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include "timber.h"

int main(void)
{
    TDR_DIAG_TRACE();

    TDR_DIAG_INFO("This is some information.\n");
    TDR_DIAG_ERROR("This is some more information.\n");

    return 0;
}

